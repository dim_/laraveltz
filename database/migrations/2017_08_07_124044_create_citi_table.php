<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cities')) {
            Schema::create('cities', function (Blueprint $table) {
                $table->bigInteger('id',true,false);
                $table->string('name');
                $table->bigInteger('country_id',false,false);
            });

            Schema::table('cities', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');;
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cities')) {
            Schema::drop('cities');
        }
    }
}
