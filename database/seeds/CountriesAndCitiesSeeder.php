<?php

use Illuminate\Database\Seeder;

class CountriesAndCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'name' => "Ukraine",
        ]);

        DB::table('countries')->insert([
            'name' => "Russia",
        ]);

        DB::table('countries')->insert([
            'name' => "Poland",
        ]);
        DB::table('cities')->insert([
            'country_id' => 1,
            'name' => "Kremenchuk",
        ]);

        DB::table('cities')->insert([
            'country_id' => 1,
            'name' => "Kiyv",
        ]);

        DB::table('cities')->insert([
            'country_id' => 1,
            'name' => "Kharkov",
        ]);

        DB::table('cities')->insert([
            'country_id' => 2,
            'name' => "Moscov",
        ]);

        DB::table('cities')->insert([
            'country_id' => 3,
            'name' => "Krakov",
        ]);
    }
}
