<?php

namespace App\Http\Controllers;

use App\Models\User;
use DB;
use Doctrine\Common\Collections\Criteria;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class WelcomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view('welcome');
    }

    /**
     *
     */
    public function countries(){
        $data = array();
        $countries = DB::table('countries')->get();
        foreach ($countries as $country){
           $data[] = array("Country"=>$country->name,"Id"=>$country->id);
        }
        return new JsonResponse($data);
    }

    public function cities($id){
        $data = array();
        $cities = DB::table('cities')->where('country_id', $id)->get();
        foreach ($cities as $city){
            $data[] = array("City"=>$city->name,"Id"=>$city->id);
        }
        return new JsonResponse($data);
    }

}
